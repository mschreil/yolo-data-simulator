import numpy as np 
import cv2 
import math 
import random
import copy
import time
import os 

from objects import Triangle
from variation import RandomMove, RandomRot, RandomScale, RandomColour
from variation import ImageNoise
from util import calc_bb
from util import save_img, save_yolo_txt 
from util import conv_time

from collections import Counter

def imbalance_measure(anz_objs_list):
    """
        1: when each image contains an object of intrest
        0: when no image contains an object of intrest 
    """

    # delete first element of anz_obs_list
    del anz_objs_list[0]

    n_imgs = len(anz_objs_list)
    count = Counter(anz_objs_list)

    #entropy_list = np.zeros(n_imgs)
    #for i, nr_obs in enumerate(anz_objs_list):
    #    p = count[nr_obs]/n_imgs
    #    entropy_list[i] = (-p*math.log2(p))
    
    imb = count[1]/n_imgs
    
    return imb

if __name__ == "__main__":
    # Simulatipon Parametrs 
    show_image = True 
    nr_imgs = 100000
    random.seed(50)
    nr_objs = 5
    del_prob = 0.4 # probability to delete object when outside 
    init_prob_1 = 0.0001 # probability to create new object, tested each loop step 
    init_prob_2 = 1.0 # how many missing objs will be create if 'init_prob' is true 

    # Save Data Parameters 
    save_data = True
    output_folder = "/home/max/Desktop/SimulatedData_high_Imbalance_3"
    base_name = "sim"

    # init Triangel Objecets 
    init_center = np.array([-1000, -1000]) #center somewhere outside of the image 
    init_a = 50
    init_b = 50
    init_c = 50 
    init_ang = 0
    init_colour = [220, 200, 100]
    #colour = init_colour
    img_size = (416, 416)

    ##### Init Movements ##### 
    ## Parameters RandomMove
    rm_shift = random.randint
    rm_diag_prob = 0.5
    
    ## Parameters RandomRot
    rr_shift = random.randint
    rr_rot_prob = 0.9 
    rr_dyn_rot = True
    
    ## Parameters RandomScale 
    rs_scale_prob = 0.5
    rs_min_scale = 0.3
    rs_max_scale = 2.5
    rs_duration = random.randint
    rs_dyn_scale = True
    
    ## Parameters RandomColour 
    rc_clr_prob = 0.5
    rc_nr_colours = nr_objs 
    rc_dyn_colour = False
    rc_shift_colour = random.randint

    seed = random.randint

    random_move = [RandomMove(rm_shift(1,3), img_size, rm_diag_prob, seed(1,1000)) for _ in range(0, nr_objs)]
    random_rot = [RandomRot(rr_rot_prob, rr_shift(1,3), img_size, rr_dyn_rot, seed(1,1000)) for _ in range(0, nr_objs)]
    random_scale = [RandomScale(rs_scale_prob, init_a, init_b, init_c, rs_min_scale, rs_max_scale, rs_duration(150,250), rs_dyn_scale, img_size, seed(1,1000)) for _ in range(0, nr_objs)]
    random_colour = [RandomColour(rc_clr_prob, rc_nr_colours, rc_dyn_colour, rc_shift_colour(1,3), img_size, seed(1,1000)) for _ in range(0, nr_objs)]
    
    #Image Noise
    img_noise = ImageNoise(mean=(0,0,0), std=(0.5, 0.5, 0.5), seed=101)

    # init Triangles 
    triangles = [Triangle(init_center, init_a, init_b, init_c, init_ang, init_colour) for _ in range(1, nr_objs+1)]

    # init Variables
    create_new = False 
    count = 0
    anz_objs_list = []

    tic = time.time() 
    while count < nr_imgs:
        background = np.ones((img_size[0], img_size[1], 3), np.uint8)*255
        # add random gausian backgournd noise 
        background = img_noise(background)

        ###### draw Triangles ###### 
        yolo_labels = []
        for n, triangle in enumerate(triangles):
            background = cv2.drawContours(background, [triangle.P], 0, triangle.colour, -1, lineType=cv2.LINE_AA)
            #backgournd = cv2.circle(background, tuple(triangle.center), int(np.rint(triangle.circ_radius)), (0, 0, 255), 1, lineType=cv2.LINE_AA) 
            x1, y1, width, height, yolo_str = calc_bb(triangle, 0, img_size)
            yolo_labels.append(yolo_str)
            if save_data is False:
                background = cv2.rectangle(background,(x1,y1), (x1+width, y1+height), (0, 255, 0), 1, lineType=cv2.LINE_AA)
        
        # get number of objs in actual image 
        anz_objs_list.append(1 if len(yolo_labels)>0 else 0)

        # Upodate Triangles with new random Parameters
        obj_to_del = [] 
        for i, triangle in enumerate(triangles):
            triangle = random_rot[i](triangle)
            triangle = random_scale[i](triangle)
            triangle = random_colour[i](triangle)
            triangle = random_move[i](triangle)
            triangles[i] = triangle        
            # randomly delete triangles 
            if random_move[i].outside is True and random.random() < del_prob and create_new is False:
                obj_to_del.append(i)

        #delete 
        print("Del Triangle: {}/{} ".format(len(obj_to_del), len(triangles))) if len(obj_to_del) is not 0 else None
        for idx in sorted(obj_to_del, reverse=True):
            del triangles[idx]
            del random_move[idx]
            del random_rot[idx]
            del random_scale[idx]
            del random_colour[idx]
        
        if len(triangles) is 0:
            create_new = True

        if len(triangles) is nr_objs:
            create_new = False 

        # create new Triangels of some are deleted 
        if  (create_new is True) and (random.random()  < init_prob_1):
            for _ in range(0, nr_objs-len(triangles)):
                if random.random() < init_prob_2:
                    # set center somewhere outside of the image 
                    triangles.append(Triangle(init_center, init_a, init_b, init_c, init_ang, init_colour))
                    # init new random movements
                    random_move.append(RandomMove(rm_shift(1,3), img_size, rm_diag_prob, seed(1,1000)))
                    random_rot.append(RandomRot(rr_rot_prob, rr_shift(1,3), img_size, rr_dyn_rot, seed(1,1000))) 
                    random_scale.append(RandomScale(rs_scale_prob, init_a, init_b, init_c, rs_min_scale, rs_max_scale, rs_duration(150,250), rs_dyn_scale, img_size, seed(1,1000)))
                    random_colour.append(RandomColour(rc_clr_prob, rc_nr_colours, rc_dyn_colour, rc_shift_colour(1,3), img_size, seed(1,1000)))
                    print("Init Triangle: {}/{} - center: {}".format(len(triangles), nr_objs ,init_center))

        if save_data: 
            name = base_name +"_"+str(count)
            save_img(output_folder, name, background)
            save_yolo_txt(output_folder, name, yolo_labels)

        count += 1 
        if count % 1000 is 0:
            print("------------------>   Time: {}   Number Images: {}".format(conv_time(time.time()-tic) ,count))

        if show_image:
            cv2.imshow("image", background)
            key = cv2.waitKey(1)
            if key==113 or key==27: # to quit press 'q' or 'esc' 
                cv2.destroyAllWindows()
                break
    
    #Calculate entropy of Dataset: 
    imb_measure = imbalance_measure(anz_objs_list)
    print("Imbalance of Dataset: {}".format(imb_measure))
            