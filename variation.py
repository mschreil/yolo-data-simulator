import numpy as np 
import cv2 
import math 
import random
import copy
import os 

from objects import Triangle

class RandomMove():
    def __init__(self, shift, img_size, diag_prob, seed=None):
        random.seed(seed)
        self.shift = shift
        self.img_size = img_size
        self.diag_prob = diag_prob
        _, self.direction = self.random_start(diag_p=self.diag_prob)
        #self.direction = "down"
        
    def __call__(self, triangle):
        assert isinstance(triangle, Triangle)        
        if (triangle.center[1]-triangle.circ_radius) < -2*triangle.circ_radius or math.sqrt((triangle.center[1]-triangle.circ_radius)**2) > self.img_size[0] or \
           (triangle.center[0]-triangle.circ_radius) < -2*triangle.circ_radius or math.sqrt((triangle.center[0]-triangle.circ_radius)**2) > self.img_size[1]:
            # Triangel is outside -> start new movement 
            self.outside = True
            new_center, self.direction = self.random_start(radius=triangle.circ_radius, diag_p=self.diag_prob)
        else:
            self.outside = False
            center = triangle.center
            if self.direction == "down":
                new_center = center + np.array([0, self.shift])
            elif self.direction == "down_diag_right":
                new_center = center + np.array([self.shift, self.shift]) 
            elif self.direction == "down_diag_left":
                new_center = center + np.array([-self.shift, self.shift])

            elif self.direction == "up":
                new_center = center - np.array([0, self.shift]) 
            elif self.direction == "up_diag_right":
                new_center = center - np.array([self.shift, self.shift]) 
            elif self.direction == "up_diag_left":
                new_center = center - np.array([-self.shift, self.shift])

            elif self.direction == "left":
                new_center = center - np.array([self.shift, 0]) 
            elif self.direction == "left_diag_up":
                new_center = center - np.array([self.shift, self.shift]) 
            elif self.direction == "left_diag_down":
                new_center = center - np.array([self.shift, -self.shift]) 

            elif self.direction == "right":
                new_center = center + np.array([self.shift, 0])
            elif self.direction == "right_diag_up":
                new_center = center + np.array([self.shift, -self.shift])
            elif self.direction == "right_diag_down":
                new_center = center + np.array([self.shift, self.shift])

            else:
                raise KeyError("Wrong Key for 'self.direction': {}".format(self.direction))
        #return new_center 
        return Triangle(new_center, triangle.a, triangle.b, triangle.c, triangle.angle, triangle.colour)
    

    def random_start(self, radius=0, diag_p=0.5):
        '''choose random start side and movement 
        '''
        side = 0 # 0,2 top, bottom; 1,3 left, right 
        # choose Start side
        while True:
            if random.random() < 0.25:
                break
            else: 
                side += 1
                side = 0 if side > 3 else side

        if side is 0:
            center = np.array([random.randint(0, self.img_size[1]), -radius]) 
            direction = "down" 
            if random.random() < diag_p:
                direction = "down_diag_right"
            if random.random() < diag_p :
                direction = "down_diag_left"
        elif side is 2: 
            center = np.array([random.randint(0 , self.img_size[1]), self.img_size[0]+radius])
            direction = "up" 
            if random.random() < diag_p :
                direction = "up_diag_right"
            if random.random() < diag_p : 
                direction = "up_diag_right"
        elif side is 1: 
            center = np.array([self.img_size[1]+radius, random.randint(0 ,self.img_size[1])]) 
            direction = "left"
            if random.random() < diag_p:
                direction = "left_diag_up"
            if random.random() < diag_p:
                direction = "left_diag_down"
        elif side is 3: 
            center = np.array([-radius, random.randint(0 ,self.img_size[1])]) 
            direction = "right"  
            if random.random() < diag_p :
                direction = "right_diag_up"
            if random.random() < diag_p :
                direction = "right_diag_up"
        else:
            raise ValueError("Wrong number for 'side': {}".format(side))
        
        return center, direction 


class RandomRot():
    def __init__(self, p, shift, img_size, dyn_rot, seed=None):
        random.seed(seed)
        self.p = p
        self.shift = shift
        self.img_size = img_size
        self.dyn_rot = dyn_rot
        self.direction = self.random_direciton(self.p)
        
    def __call__(self, triangle): 
        assert isinstance(triangle, Triangle)        
        if (triangle.center[1]-triangle.circ_radius) < (-2*triangle.circ_radius) or math.sqrt((triangle.center[1]-triangle.circ_radius)**2) > self.img_size[0] or \
           (triangle.center[0]-triangle.circ_radius) < (-2*triangle.circ_radius) or math.sqrt((triangle.center[0]-triangle.circ_radius)**2) > self.img_size[1]:
            # chhose new rotaiton if triangle out of image 
            self.direction = self.random_direciton(self.p)
        
        if self.direction is "clock":
            new_angle = triangle.angle+self.shift
            if self.dyn_rot and new_angle > 360:
                new_angle = new_angle-360
                self.direction = self.random_direciton(self.p)
        elif self.direction is "cclock":
            new_angle = triangle.angle-self.shift
            if self.dyn_rot and new_angle < -360:
                new_angle = new_angle+360
                self.direction = self.random_direciton(self.p)
        elif self.direction is "no_rot":
            new_angle = triangle.angle
        else:
            raise KeyError("Wrong Key for 'self.direction': {}".format(self.direction))    
        #return new_angle
        return Triangle(triangle.center, triangle.a, triangle.b, triangle.c, new_angle, triangle.colour)


    def random_direciton(self, p):
        if random.random() < p:
            if random.random() < 0.5:
                direction = "clock"
            else: 
                direction = "cclock"
        else:
            direction = "no_rot"        
        return direction 


class RandomScale():
    def __init__(self, p, init_a, init_b, init_c, min_scale, max_scale, duration, dyn_scale, img_size, seed=None):
        random.seed(seed)
        self.p = p
        self.init_a = init_a
        self.init_b = init_b
        self.init_c = init_c
        self.min_scale = min_scale
        self.max_scale = max_scale
        self.duration = duration
        self.dyn_scale = dyn_scale
        self.img_size = img_size
        self.scaling = self.random_up_down()
        self.factor_gen = self.get_factor(self.scaling, self.min_scale, self.max_scale, self.duration, dyn_scale=self.dyn_scale)          

    def __call__(self, triangle):
        assert isinstance(triangle, Triangle)
        
        if (triangle.center[1]-triangle.circ_radius) < (-2*triangle.circ_radius) or math.sqrt((triangle.center[1]-triangle.circ_radius)**2) > self.img_size[0] or \
           (triangle.center[0]-triangle.circ_radius) < (-2*triangle.circ_radius) or math.sqrt((triangle.center[0]-triangle.circ_radius)**2) > self.img_size[1]:
            # if Object is outside
            if random.random() < self.p:
                self.scaling = self.random_up_down()
                self.factor_gen = self.get_factor(self.scaling, self.min_scale, self.max_scale, self.duration,  dyn_scale=self.dyn_scale, start=True)          
            else: factor = 1
        
        try: 
            factor = next(self.factor_gen)
        except StopIteration: 
            self.scaling = "up" if self.scaling is "down" else "down"               
            self.factor_gen = self.get_factor(self.scaling, self.min_scale, self.max_scale, self.duration,  dyn_scale=self.dyn_scale)
            factor = next(self.factor_gen)          
        
        new_a = int(self.init_a*factor+0.5) if int(self.init_a*factor+0.5) > 0 else 1 
        new_b = int(self.init_b*factor+0.5) if int(self.init_b*factor+0.5) > 0 else 1 
        new_c =  int(self.init_c*factor+0.5) if int(self.init_c*factor+0.5) > 0 else 1 

        #return new_a, new_b, new_c   
        return Triangle(triangle.center, new_a, new_b, new_c, triangle.angle, triangle.colour)
       
                               
    def random_up_down(self):
        if random.random() < 0.5:
            scaling = "up"
        else:
            scaling = "down"
        return scaling 

    def get_factor(self, scaling, min_scale, max_scale, duration, dyn_scale=True, start=False):
        if dyn_scale:
            # dont start new movment with size greater than original size

            if start: 
                if scaling is "up":
                    factors = np.linspace(1.0, max_scale, duration//2)      
                elif scaling is "down":
                    factors = np.linspace(min_scale, 1.0, duration//2)      
                else:
                    raise KeyError("Wrong Key for 'scaling': {}".format(scaling))
            else:
                factors = np.linspace(min_scale, max_scale, duration)      

            for i in range(len(factors)):
                if scaling is "up":
                    new_factor = factors[i]     
                elif scaling is "down":
                    new_factor = factors[len(factors)-(i+1)]
                elif scaling is None:
                    new_factor = 1.0
                else: 
                    raise KeyError("Wrong Key for 'scaling': {}".format(scaling))
                yield new_factor 
        else:
            new_factor = random.uniform(min_scale, max_scale)
            while True:
                yield new_factor


class RandomColour():
    def __init__(self, p, nr_colours, dyn_colour, shift_col, img_size, seed=None):
        np.random.seed(seed)
        random.seed(seed)
        self.p = p
        self.nr_colours = nr_colours
        self.dyn_colour = dyn_colour
        self.shift_col = shift_col
        self.img_size = img_size
        self.colours = np.random.rand(self.nr_colours, 3)*255
        
        #init Variables
        self.new_colour = self.colours[random.randint(0, (self.nr_colours-1))]
        self.ch_marker = [0,0,0]

    def __call__(self, triangle):
        if (triangle.center[1]-triangle.circ_radius) < (-2*triangle.circ_radius) or math.sqrt((triangle.center[1]-triangle.circ_radius)**2) > self.img_size[0] or \
           (triangle.center[0]-triangle.circ_radius) < (-2*triangle.circ_radius) or math.sqrt((triangle.center[0]-triangle.circ_radius)**2) > self.img_size[1]:
            # if Object is outside 
            self.ch_marker = [0,0,0]
            if random.random() < self.p:
                self.new_colour = self.colours[random.randint(0, (self.nr_colours-1))]
            else:
                self.new_colour = triangle.colour
        else:
            if self.dyn_colour:
                channel = random.randint(0,2)
                if self.ch_marker[channel] is 0:
                    self.new_colour[channel] = triangle.colour[channel] + self.shift_col 
                    if self.new_colour[channel] > 255:
                        self.ch_marker[channel] = 1
                else: 
                    self.new_colour[channel] = triangle.colour[channel] - self.shift_col
                    if self.new_colour[channel] < 0:
                        self.ch_marker[channel] = 0
            else:
                self.new_colour = triangle.colour
        #return np.rint(self.new_colour).astype(int).tolist()
        return Triangle(triangle.center, triangle.a, triangle.b, triangle.c, triangle.angle, np.rint(self.new_colour).astype(int).tolist())

class ImageNoise(): 
    def __init__(self, mean, std, seed):
        np.random.seed(seed)
        random.seed(seed)
        self.mean = mean
        self.std = std 

    def __call__(self, image):
        image = self.gaussian_noise(image, self.mean, self.std)
        return image

    def gaussian_noise(self, image, mean, std): 
        image1 = copy.deepcopy(image)
        cv2.randn(image1, mean, std)
        return image+image1

#def random_form()
#def random_light()