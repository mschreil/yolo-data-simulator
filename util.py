import numpy as np 
import cv2 
import math 
import random
import copy
import os 



def calc_bb(triangle, obj_id, img_size):
    x1 = triangle.P[:,0].min()
    y1 = triangle.P[:,1].min()
    x2 = triangle.P[:,0].max()
    y2 = triangle.P[:,1].max()

    # shrink to BB to image size
    x1 = min(max(x1,0), img_size[1])
    y1 = min(max(y1,0), img_size[0])
    x2 = min(max(x2,0), img_size[1])
    y2 = min(max(y2,0), img_size[0])

    # calculate width and height
    width = (x2-x1)
    height = (y2-y1)
    #print("P1({:4}|{:4}) - P2({:4}|{:4})".format(x1,y1,x2,y2))
    yolo_str = "{} {} {} {} {}".format(obj_id, (x1+(width/2))/img_size[1], (y1+(height/2))/img_size[0], width/img_size[1], height/img_size[0])
    return x1, y1, width, height, yolo_str

def save_yolo_txt(output_folder, name, yolo_labels):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    if os.path.splitext(name)[1] is not ".txt":
        name = os.path.splitext(name)[0] + ".txt"
    
    with open(os.path.join(output_folder, name), "w") as f:
        for label in yolo_labels:
            f.write(label+"\n")

def save_img(output_folder, name, img): 
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    if os.path.splitext(name)[1] is not ".png":
        name = os.path.splitext(name)[0] + ".png"
    cv2.imwrite(os.path.join(output_folder,name), img)

def conv_time(seconds):
    '''
    convert seconds to hours, minutes and seconds
    
    Arguments:
    ---------
        seconds (int, time): Seconds to convert
    
    Return:
    --------
        (string): "{}h {}min {}sec"
    '''
    
    if not isinstance(seconds, int):
        seconds = int(seconds)
        
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    return "{:2}h {:2}min {:2}sec".format(hours, minutes, seconds)