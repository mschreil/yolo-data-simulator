import numpy as np 
import cv2 
import math 
import random


class Triangle():
    def __init__(self, center, a, b, c, angle, colour):
        assert (a+b)>c, "Entered triangle sides do not satisfy the triangle inequality (a+b)>c"
        self.center = center 
        self.a = a
        self.b = b
        self.c = c 
        self.angle = angle 
        self.colour = colour
        
        P0 = np.array([0, 0])
        P1 = np.array([0, self.a])
        p2_y = ((self.a**2)+(self.c**2)-(self.b**2))/(2*self.a)
        p2_x = math.sqrt((self.c**2)-(p2_y*p2_y))
        P2 = np.array([p2_x, p2_y])
        self.P = np.array([P0,P1,P2])
        
        center_0 = self.calc_center()
        self.trans(self.center-center_0)
        self.rot(angle)

        self.area = self.calc_area()
        self.circ_radius = self.calc_circumcircle()
    
    def rot(self, angle):
        rad = angle*(math.pi/180)
        rot_mat = np.array([[math.cos(rad), -math.sin(rad)],
                            [math.sin(rad), math.cos(rad)]])

        self.trans(-self.calc_center())
        self.P = np.array([np.dot(rot_mat, self.P[0]), 
                           np.dot(rot_mat, self.P[1]), 
                           np.dot(rot_mat, self.P[2])])
        self.trans(self.center)
        self.P = np.rint(self.P).astype(int)

    def trans(self, t):
        self.P = np.rint(np.array([self.P[0]+t, self.P[1]+t, self.P[2]+t])).astype(int)
    
    def calc_center(self):
        return ((self.P[0]+self.P[1]+self.P[2])/3).astype(int)

    def calc_circumcircle(self):
        return np.rint((self.a*self.b*self.c)/(4*self.area)).astype(int)
    
    def calc_area(self):
        AB = self.P[1] - self.P[0]
        AC = self.P[2] - self.P[0]
        area = 0.5 * np.linalg.det(np.array([AC,AB]))
        return area

        
