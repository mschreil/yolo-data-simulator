# Dataset Simulator

<p align="center">
  <img width="605" height="455" src="/figures/sim_sample_2.jpg">
</p>

The simulator creates a stream of moving isosceles triangles. 
Each triangle gets initialized with a randomly chosen size, color, and movement speed.
While moving, each object changes its scale dynamically or holds its original shape with a probability of $0.5$. 
Furthermore, each triangle has a probability of rotating clockwise or counterclockwise with an individual randomly chosen speed. 
If an object moves completely out of the image, it gets deleted by a certain delete-probability. 
If it gets out of the images and is not deleted, it will change its movement behavior 
(i.e., movement speed, scaling, and if desired rotational speed). 
When all triangles are deleted, the simulator creates new triangles with a certain create-probability.
Moreover, the objects are not restricted in their space of movement. 
They are able to move above or below each other, which could lead to occlusions. 
Furthermore, the background includes random gaussian color noise to ensure variants in the individual images, even if there are no objects present. 


```bash
pip install opencv-python
```
